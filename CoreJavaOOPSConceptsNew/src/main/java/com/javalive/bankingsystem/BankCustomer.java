package com.javalive.bankingsystem;

public class BankCustomer {// Cohesion example
	private int custID;
	private String custName;
	private String custAddress;
	private long custContactNumber;
	private long custAadharID;
	private String custPAN;
	
	
	
	public BankCustomer() {
		super();
	}//Please note that this non parameterized constructor and below defined parameterized constructor
	 //are example of constructor overloading as they have different signatures.
	
	public BankCustomer(int custID, String custName, String custAddress, long custContactNumber, long custAadharID,
			String custPAN) {
		super();
		this.custID = custID;
		this.custName = custName;
		this.custAddress = custAddress;
		this.custContactNumber = custContactNumber;
		this.custAadharID = custAadharID;
		this.custPAN = custPAN;
		System.out.println("Bank customer dtata added successfully.");
	}

	public int getCustID() {
		return custID;
	}

	public void setCustID(int custID) {
		this.custID = custID;
	}

	public String getCustName() {
		return custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getCustAddress() {
		return custAddress;
	}

	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}

	public long getCustContactNumber() {
		return custContactNumber;
	}

	public void setCustContactNumber(long custContactNumber) {
		this.custContactNumber = custContactNumber;
	}

	public long getCustAadharID() {
		return custAadharID;
	}

	public void setCustAadharID(long custAadharID) {
		this.custAadharID = custAadharID;
	}

	public String getCustPAN() {
		return custPAN;
	}

	public void setCustPAN(String custPAN) {
		this.custPAN = custPAN;
	}
}
