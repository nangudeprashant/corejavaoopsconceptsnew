package com.javalive.bankingsystem;

import java.util.List;

/**
 * @author www.itaspirants.com
 * description This is the base class of account classes of Banking System application.
 * Go through it and read the comments provided very carefully thereby try to
 * understand the OOPS concept of Java. 
 */
public abstract class BankAccount{
	private double accountBalance;// Encapsulation
	private BankCustomer accountHolder;//has-a relationship i.e. association among classes.
	private long accountNumber;				//This is also example of coupling.
	
	public BankAccount() {
		System.out.println("Constructor of BankAccount class.");//This is for explaining constructor chaining.
	}
	//Please note that this non parameterized constructor and below defined parameterized constructor
	//are example of constructor overloading as they have different signatures.
	public BankAccount(long accountNumber,BankCustomer accountHolder,double accountBalance) {
		super();
		System.out.println("In the parameterized constructure of base class BankAccount.");
		this.accountNumber = accountNumber;
		this.accountHolder = accountHolder;
		this.accountBalance = accountBalance;
		System.out.println("Congrats! Account created successfully.");
	}

	public double getAccountBalance() {// Encapsulation
		System.out.println("In the getAccountBalance method of base class BankAccount.");
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {// Encapsulation
		this.accountBalance = accountBalance;
	}

	public BankCustomer getAccountHolder() {// Encapsulation
		return accountHolder;
	}

	public void setAccountHolder(BankCustomer accountHolder) {// Encapsulation
		this.accountHolder = accountHolder;
	}

	public long getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(long accountNumber) {
		this.accountNumber = accountNumber;
	}

	public boolean depositAmt(double amount) {
		System.out.println("In the depositAmt method of base class BankAccount with single paramater amount.");
		if (amount <= 0) {
			System.out.println("Invalid amount");
			return false;
		} else {
			this.accountBalance = this.accountBalance + amount;
			System.out.println("Amount deposited successfully.");
			this.displayBalanceAmt();
			return true;
		}
	}

	public abstract boolean withdrawAmt(double amount);//Abstaction: Method with only declaration and without implementation.

	public void displayBalanceAmt() {
		System.out.println("In the displayBalanceAmt method of base class BankAccount.");
		System.out.println("Account balance is "+this.getAccountBalance());
	}
	
	public boolean transferAmount(double amount, long accountNumber) {
		System.out.println("In the transferAmount method.");
		boolean result = false;
		if (this.getAccountBalance() >= amount) {
			this.setAccountBalance(this.getAccountBalance() - amount);
			//Here is the code to get particular account number details and
			// add the said amount to its balance.
			System.out.println("Amount transfered successfully to said account.");
			this.displayBalanceAmt();
			result = true;
		} else {
			System.out.println("Sorry! Insufficient fund in the account.");
		}
		return result;
	}
}
