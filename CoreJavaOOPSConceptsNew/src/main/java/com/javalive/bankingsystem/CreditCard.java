package com.javalive.bankingsystem;

public interface CreditCard {
	boolean transferAmount(double amount, long accountNumber, String PaymentGatewayDetails);
}
