/**
 * 
 */
package com.javalive.bankingsystem;

import java.awt.List;

/**
 * @author www.itaspirants.com
 *
 */
class BankingOperations {
	public static void main(String[] args) {

		System.out.println("=============================Saving account operations===================================================");
		//Creating BankCustomer class's object for saving account. Here we are using parameterized constructor to create this object.  
		BankCustomer savingAccountHolder = new BankCustomer(1, "SavingAccountHolderName", "SavingAccountHolderAddress",
				9890167608L, 111111111, "AIDPN9742B");
		/*
		 * We can also create the same object by following way, it is depends upon the requirement of the application.
		 * BankCustomer savingAccountHolder = new BankCustomer();
		 * SavingAccount savingAccount=new SavingAccount();
		 * savingAccount.setAccountNumber(123);
		 * savingAccount.setAccountHolder(savingAccountHolder);
		 * savingAccount.setAccountBalance(00.00);
		 */
		
		//Creating SavingAccount class's object. Here we are using parameterized constructor to create this object.
		SavingAccount savingAccount = new SavingAccount(123, savingAccountHolder, 0.0);
		savingAccount.depositAmt(1000.00);

		savingAccount.transferAmount(800, 2354);
		savingAccount.withdrawAmt(1200);
		savingAccount.depositAmt(10000.00);
		savingAccount.withdrawAmt(1200);
		
		System.out.println("\n=====================Current account operations=============================================");
		BankCustomer currentAccountHolder = new BankCustomer(2, "CurrentAccountHolderName", "CurrentAccountHolderAddress",
				9890167606L, 123456789, "AIDPN9742C");
		
		CurrentAccount currentAccount=new CurrentAccount(456, currentAccountHolder, 0.0);
		currentAccount.depositAmt(8000);
		
		currentAccount.transferAmount(4500, 123);
		currentAccount.depositAmt(100000);
		currentAccount.transferAmount(4500, 123);
		currentAccount.withdrawAmt(93000);
		
		System.out.println("\n=====================Fixed deposit account operations=======================================");
		BankCustomer fdAccountHolder = new BankCustomer(3, "FDAccountHolderName", "FDAccountHolderAddress",
				9890167609L, 112233445, "AIDPN9742D");
		
		 FixedDepositAccount fdAccount=new FixedDepositAccount(789,fdAccountHolder,0.0,456);
		 fdAccount.depositAmt(100000);
		 
		 fdAccount.withdrawAmt(20,000);
		 fdAccount.transferAmount(100000.0, 456);
		 fdAccount.withdrawAmt(100000);
		 fdAccount.withdrawAmt(100000.0,456l);
	}
}
