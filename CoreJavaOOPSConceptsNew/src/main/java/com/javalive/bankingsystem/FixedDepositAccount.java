package com.javalive.bankingsystem;

public class FixedDepositAccount extends BankAccount {
    
	private long referenceAccountNumber;
	
	public FixedDepositAccount() {
		super();
		// TODO Auto-generated constructor stub
	}

	public FixedDepositAccount(long accountNumber, BankCustomer accountHolder, double accountBalance,long referenceAccountNumber) {
		super(accountNumber, accountHolder, accountBalance);
		this.referenceAccountNumber = referenceAccountNumber;
		}

	@Override
	public boolean withdrawAmt(double amount) {//abstraction i.e. implementation of abstract method of base class.
		//this.setAccountBalance(this.getAccountBalance()-amount);
		System.out.println("In withdrawAmt() method of FixedDepositAccount class.");//this is only for understanding at console in program output.
		System.out.println("Invalid operation for this type of account."
				+ "Amount can be withdrawn only after desolviinng the FD."
				+ "\nIf wants to do so, please provide account number to transfer the amount.");
		return false;
	}
	
	public boolean withdrawAmt(double amount, long accountNumber) {// Polymorphism : Method over loading i.e. define new method having same name but different signature than that of defined in base class.
		//here call amountTransfer method to transfer the said amount to the said account.
		System.out.println("In the over loaded withdrawAmt method of derived class FixedDepositAccount with additional parameter accountNumber.");
		boolean result=false;
		if(this.getAccountBalance()== amount) {
			super.transferAmount(amount, accountNumber);
			result=true;
		}
		else{
			System.out.println("Invalid amount.....");
		}
		return result;
	}

	@Override
	public boolean transferAmount(double amount,long accountNumber) {// Polymorphism : Method overriding i.e. redefining method already defined in base class.
		System.out.println("In the overridden transferAmount method of derived class FixedDepositAccount.");//this is only for understanding at console in program output.
		System.out.println("This operation for this type of account is only possible after desolving the FD."
				+ "\nIf you wants to do so, then please make use of withdraw amount method of this class and provide details"
				+ "\n like amt and account number to tranfer the amount.");
		return false;
	}
	
	public long getReferenceAccountNumber() {
		return referenceAccountNumber;
	}

	public void setReferenceAccountNumber(long referenceAccountNumber) {
		this.referenceAccountNumber = referenceAccountNumber;
	}

}
