package com.javalive.bankingsystem;

public class SavingAccount extends BankAccount implements CreditCard{// is a relationship i.e. inheritance.

	public SavingAccount() {
		super();//Constructor chaining with BankAccount class.
	}

	public SavingAccount(long accountNumber, BankCustomer accountHolder, double accountBalance) {
		super(accountNumber, accountHolder, accountBalance);//Constructor chaining with BankAccount class.
	}

	@Override
	public boolean withdrawAmt(double amount) {// abstraction i.e. implementation of abstract method of base class.
		System.out.println("In the withdrawAmt() method of SavingAccount class.");//this is only for understanding at console in program output.
		System.out.println("Amount to be withdraw is "+amount);
		boolean result = false;
		if (this.getAccountBalance() > amount) {
			this.setAccountBalance(this.getAccountBalance() - amount);
			this.displayBalanceAmt();
			result = true;
		} else {
			System.out.println("Sorry! Insufficient fund in the account.");
		}
		return result;
	}
	//Implementation of the method from CrediCard interface.
	public boolean transferAmount(double amount, long accountNumber, String PaymentGatewayDetails) {
		// Now the logic for transferring the amount to said account is goes here.
		//This method will be called by external system like payment gateway in real time.
		//Web service is generally used by the calling system to call it.
		//Thats why we have created an interface in order to facilitates the contract
		//and business logic secrecy like how to apply any discount coupon between our system and the calling payment gateway system. 
		return false;
	}

}
