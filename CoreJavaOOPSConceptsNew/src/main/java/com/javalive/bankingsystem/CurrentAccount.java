package com.javalive.bankingsystem;

public class CurrentAccount extends BankAccount {//is a relationship i.e. inheritance.
	private int minimumBalance = 5000;

	public CurrentAccount() {
		super();// Constructor chaining with BankAccount class.
		// TODO Auto-generated constructor stub
	}
	
	public CurrentAccount(long accountNumber, BankCustomer accountHolder, double accountBalance) {
		super(accountNumber, accountHolder, accountBalance);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean withdrawAmt(double amount) {//abstraction i.e. implementation of abstract method of base class.
		System.out.println("In the withdrawAmt() method of CurrentAccount class.");//this is only for understanding at console in program output.
		System.out.println("Amount to be withdraw is "+amount);
		boolean result = false;//Please note that this logic is different that that of implemented in saving accounts withdrawAmt() method where we have also included minimum balance.
		if ((getAccountBalance() - this.minimumBalance) > amount) {
			this.setAccountBalance(this.getAccountBalance() - amount);
			this.displayBalanceAmt();
			result=true;
		} else {
			System.out.println("Insufficient funds......");
			this.displayBalanceAmt();
		}
		return result;
	}

	public boolean transferAmount(double amount, long accountNumber) {// Polymorphism : Method overriding i.e. redefining method already defined in base class.
		System.out.println("In the transferAmount() method of CurrentAccount class.");//this is only for understanding at console in program output.
		System.out.println("Amount to be transffered is "+amount);
		boolean result = false;
		if ((this.getAccountBalance()-this.minimumBalance) > amount) {//here we have made changes to maintain minimum balance in the current account
			this.setAccountBalance(this.getAccountBalance() - amount);
			
			//Here is the code to get particular account number details and
			// add the said amount to its balance.
			this.displayBalanceAmt();
			result = true;
		} else {
			System.out.println("Sorry! Insufficient fund in the account.");
		}
		return result;
	}
	
	public void displayBalanceAmt() {// Polymorphism : Method overriding i.e. redefining method already defined in base class.
		System.out.println("In the overridden displayBalanceAmt method of derived class CurrentAccount.");
		System.out.println("Account balance is " + this.getAccountBalance());
		System.out.println("Over draft amount is ........... and the due date is........");
		System.out.println("Rate of interest for OD amount is ......");
	}
	
}
