package com.javalive.association;


/*
Composition is a restricted form of Aggregation in which two entities are highly dependent on each other.  
It represents part-of relationship.
When there is a composition between two entities, the composed object cannot exist without the other entity.
*/

//This is only for understanding purpose hence only class design is provided without actual code.

class House {

}

class Room {

}

public class CompositionAssociationExample {

}
